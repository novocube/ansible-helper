# Ansible Helper 
Ansible helper is a Docker Container provided bei Novotec. It's purpose is to wrap ansible-playbook inside Docker.
Running inside of a Container avoids Version Conflicts, Missing Libraries for both System and Python. 
Given this you are able to run playbooks everywhere if you have Docker Binary installed without installing additional litter on the host.

## Docker (optional) 
If you want to build from local Dockerfile.

    docker build -t registry.gitlab.com/novocube/ansible-helper .

## Run
### Normal playbooks
Run normal playbook inside docker container.
    
    ./ansible-helper.sh -i inventory play.yml 

### Add ssh key
Add ssh key (currently loads id_rsa)
    
    ./ansible-helper-ssh-add.sh

Override key to be loaded.    

     sh ansible-helper-ssh-add-key.sh -i my_key

### SSH enabled playbooks
Run ssh enabled playbooks.
    ./ansible-helper-ssh.sh -i inventory play.yml
    
Use bash to connect run container you can use to run playbooks. 
Like this you can benefit from ansible caching.
     
    ./ansible-helper-ssh.sh bash
    bash-4.4# ansible-playbook -i inventory play.yml

# Todo

Take a look at https://github.com/ansible/ansible-runner/blob/master/Dockerfile    
    

 