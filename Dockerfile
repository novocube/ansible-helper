FROM python:3.7-alpine

ENV ANSIBLE_VERSION 2.9

ENV BUILD_PACKAGES \
  curl \
  openssh-client \
  sshpass \
  ca-certificates

ENV LD_LIBRARY_PATH /usr/local/lib
ENV LD_LIBRARY_PATH LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

# If installing ansible@testing
#RUN \
#	echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> #/etc/apk/repositories
RUN mkdir -p /ansible/playbooks/logs/
RUN mkdir -p /ansible/gem_roles

RUN set -x && \
    apk update && apk upgrade && \
    echo "==> Adding build-dependencies..."  && \
    apk --update add --virtual build-dependencies \
      libgit2-dev \
      vim \
      docker \
      git \
      curl \
      gcc \
      bash \
      g++ \
      unzip \
      tar \
      openssh \
      make \
      musl-dev \
      libgit2 \
      libffi-dev \
      apache2-utils \
      openssl-dev

RUN apk add --no-cache \
  rsync \
  ruby \
  nodejs \
  postgresql-client \
  postgresql-dev \
  ca-certificates \
  ruby-dev \
  build-base \
  bash \
  linux-headers \
  zlib-dev \
  libxml2-dev \
  libxslt-dev \
  tzdata \
  && rm -rf /var/cache/apk/*

RUN gem update --system 2.6.10 --no-rdoc --no-ri
RUN gem install bundler --no-rdoc --no-ri

      #py3-pygit2 cmake clang clang-dev make gcc libc-dev \
      #linux-headers bash git openssh alpine-sdk \
      #build-base abuild binutils git python3-dev

RUN gem update bundler
RUN echo "==> Adding Python runtime..."  && \
    apk add --no-cache ${BUILD_PACKAGES} && \
    rm -rf /var/cache/apk/*

RUN python -m pip install --upgrade pip
RUN python -m pip install cryptography==2.4.2
RUN python -m pip install pynacl python-keyczar docker openshift jmespath awscli boto3 mitogen ansible-lint
#RUN python -m pip install pygit2==0.27.4
RUN echo "==> Installing Ansible..."  && \
    python -m pip install ansible==${ANSIBLE_VERSION} ansible-inventory-grapher && \
    \
    #echo "==> Cleaning up..."  && \
    #apk del build-dependencies && \
    #rm -rf /var/cache/apk/* && \
    \
    echo "==> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible /ansible && \
    echo "[local]" >> /etc/ansible/hosts && \
    echo "localhost" >> /etc/ansible/hosts

ENV ANSIBLE_GATHERING smart
ENV ANSIBLE_HOST_KEY_CHECKING false
ENV ANSIBLE_RETRY_FILES_ENABLED false
ENV ANSIBLE_ROLES_PATH /ansible/playbooks/roles
ENV ANSIBLE_SSH_PIPELINING True
#ENV PYTHONPATH /ansible/lib
ENV PATH /ansible/bin:$PATH
ENV ANSIBLE_LIBRARY /ansible/playbooks/library
ENV DEFAULT_MODULE_PATH /ansible/playbooks/library
#ENV ANSIBLE_LIBRARY /usr/local/lib/python3.7/site-packages

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.0/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

WORKDIR /ansible/playbooks

RUN ln -s /usr/local/bin/python3 /usr/bin/python
RUN ln -s /usr/local/bin/python3 /usr/bin/python3

#RUN ldconfig /usr/local/bin
#RUN echo $DEFAULT_MODULE_PATH

#ENTRYPOINT ["/bin/bash"]
CMD ["ansible-playbook"]
