#!/bin/bash

# Output colors
underline='\033[4;37m'
purple='\033[0;35m'
bold='\033[1;37m'
green='\033[0;32m'
cyan='\033[0;36m'
red='\033[0;31m'
nc='\033[0m'

# Find image id
image=$(docker images|grep docker-ssh-agent|awk '{print $3}')

# Find agent container id
id=$(docker ps -a|grep ssh-agent|awk '{print $1}')

# Stop command
if [ "$1" == "-s" ] && [ $id ]; then
  echo -e "Removing ssh-keys..."
  docker run --rm --volumes-from=ssh-agent -it registry.gitlab.com/novocube/ssh-agent:latest ssh-add -D
  echo -e "Stopping ssh-agent container..."
  docker rm -f $id
  exit
fi

if [ $id ]; then
  docker rm -f $id
fi

# Run ssh-agent
echo -e "${bold}Launching ssh-agent container...${nc}"
docker run -d --name=ssh-agent registry.gitlab.com/novocube/ssh-agent:latest

if [ "$1" == "-i" ]; then
  key=$2
else
  key=id_rsa
fi

echo -e "Adding your ssh keys to the ssh-agent container..."
docker run --rm --volumes-from=ssh-agent -v ~/.ssh:/.ssh -it registry.gitlab.com/novocube/ssh-agent:latest ssh-add /root/.ssh/$key

echo -e "${green}ssh-agent is now ready to use.${nc}"