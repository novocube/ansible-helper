#!/bin/bash
docker pull registry.gitlab.com/novocube/ansible-helper

path=$(bundle show ansible-roles)
roles_path=$path/roles

if [[ $path == *"gems"* ]]; then
  volume="-v $roles_path:/ansible/gem_roles"
  roles_paths="/ansible/playbooks/roles:/ansible/galaxy_roles:/ansible/gem_roles"
else
  volume=""
  roles_paths="/ansible/playbooks/roles:/ansible/galaxy_roles"
fi

docker run --rm -it --name ansible-helper \
    -e ANSIBLE_ROLES_PATH=$roles_paths \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v ~/.ssh:/root/.ssh \
    -v $(pwd):/ansible/playbooks \
    -v $(pwd)/tmp/ansible-roles:/ansible/galaxy_roles \
    $volume \
    -v /var/log/ansible/ansible.log \
    -e SSH_AUTH_SOCK=/.ssh-agent/socket \
    --volumes-from=ssh-agent \
    registry.gitlab.com/novocube/ansible-helper "$@"
